class VMachine
  attr_accessor :ip_address, :port, :shared_directory, :iso
  def initialize(ip_address, port, shared_directory, iso)
    @ip_address = ip_address
    @port = port
    @shared_directory = shared_directory
    @iso = iso
  end
end

